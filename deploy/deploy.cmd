echo "now running script..."
gcloud functions deploy ithermo-remote-config --runtime=java8 --entry-point=ThermoConfigTrigger.dbTrigger --verbosity debug --trace-log --trigger-event providers/cloud.firestore/eventTypes/document.write --trigger-resource projects/ithermo/databases/(default)/documents/messages/{deviceId} --source=jar-deployment
