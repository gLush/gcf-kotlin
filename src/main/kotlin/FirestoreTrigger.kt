

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential.getApplicationDefault
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport

import com.google.api.client.http.HttpRequestInitializer
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.cloudiot.v1.CloudIot
import com.google.api.services.cloudiot.v1.CloudIotScopes
import com.google.api.services.cloudiot.v1.model.SendCommandToDeviceRequest

import com.google.firebase.FirebaseApp
import com.google.gson.JsonElement
import java.util.*
import java.util.logging.Logger


const val loggingEnabled = true

const val PROJECT_ID = "iThermo"
const val CLOUD_REGION = "us-central1"
const val REGISTRY_NAME = "vilia-registry"
const val DEVICE_ID = "ITERMO6"


class ThermoConfigTrigger {

    companion object {
        var log: Logger = Logger.getLogger(ThermoConfigTrigger::class.java.name)

        init {
            FirebaseApp.initializeApp()
        }
    }

    fun dbTrigger(jsonParam: JsonElement /* , context: Context */) {
        logIfEnabled("---> Function invoke")

//        val value = jsonParam
//                .asJsonObject.get("value")
//                .asJsonObject.get("fields")
//                .asJsonObject.get("CFG")
//                .asJsonObject.get("stringValue")
//
//        logIfEnabled("---> VALUE:${value}")

        sendCommand("CFG06A2A03A1577048400")

    }

    private fun logIfEnabled(msg: String) {
        if (loggingEnabled)
            log.info(msg)
    }

    fun sendCommand(commandStr: String) {
        val credential = getApplicationDefault().createScoped(CloudIotScopes.all())
        System.out.printf("Credential: %s\n", credential.toString())
        val jsonFactory: JsonFactory = JacksonFactory.getDefaultInstance()
        val init: HttpRequestInitializer = RetryHttpInitializerWrapper(credential)
        val service = CloudIot.Builder(GoogleNetHttpTransport.newTrustedTransport(), jsonFactory, init)
                .setApplicationName("ithermo")
                .build()
        val devicePath = String.format("projects/%s/locations/%s/registries/%s/devices/%s",
                        PROJECT_ID, CLOUD_REGION, REGISTRY_NAME, DEVICE_ID)

        val req = SendCommandToDeviceRequest()

        val encoder: Base64.Encoder = Base64.getEncoder()
        val encPayload: String = encoder.encodeToString(commandStr.toByteArray())
        req.binaryData = encPayload
        System.out.printf("Sending command to %s\n", devicePath)

        val res = service
                .projects()
                .locations()
                .registries()
                .devices()
                .sendCommandToDevice(devicePath, req)
                .execute()

        println("Command response: $res")
    }

}

